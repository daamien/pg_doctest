CHANGELOG
===============================================================================

20221129 : 0.0.1 - Init
-------------------------------------------------------------------------------

* Basic implementation
* Unit tests done with pg_doctest
* Multi-line support
* PGXN package

