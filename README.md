# pg_doctest

[doctest] for Postgres functions !

Document and Test your SQL code all at once...

[doctest]: https://docs.python.org/3/library/doctest.html

## Example

Add your unit tests directly as comments inside the function body:

```sql
CREATE OR REPLACE FUNCTION foo.add ( a INT, b INT)
RETURNS INT LANGUAGE SQL
AS $$
    -- >>> SELECT foo.add(1,1)
    -- 2
    -- >>> SELECT foo.add(NULL,9)
    -- NULL
    -- >>> SELECT foo.add(4,2)
    -- 42
    SELECT a+b;
$$;
```
Now run pg_doctest for the function inside the `foo` schema:

```sql
SELECT tests.runtests('doctest'::NAME);
 function_name | passed | failed
---------------+--------+--------
 foo.add       |      2 |      1
```

## Install

First [Install pgTap]

[Install pgTap]: https://pgtap.org/documentation.html#installation

Then build the extension

```sh
make
make install
```

Once the extension is installed on the server,
you can create it inside the database.

```sql
CREATE SCHEMA doctest;
CREATE EXTENSION pg_doctest WITH SCHEMA doctest CASCADE;
```


