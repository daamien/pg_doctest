--
-- https://en.wikipedia.org/wiki/Eating_your_own_dog_food
--
BEGIN;

CREATE SCHEMA doctest;

CREATE EXTENSION pgtap;

CREATE EXTENSION pg_doctest WITH SCHEMA doctest;

SELECT plan(9);

SELECT doctest.doctest_runtests('doctest',NULL);

SELECT finish();

ROLLBACK;
