BEGIN;

CREATE SCHEMA test;
CREATE EXTENSION pg_doctest SCHEMA test;

SELECT passed FROM test.test_call('SELECT 1','1');

CREATE SCHEMA z;
CREATE FUNCTION z.is_bob(n TEXT) RETURNS BOOLEAN LANGUAGE SQL
AS $$
-- >>> WITH person AS (
-- >>>   SELECT 'alice' AS firstname
-- >>> )
-- >>> SELECT z.is_bob(p.firstname)
-- >>> FROM person p;
-- false

SELECT n = 'bob'
$$;

SELECT * FROM test.run('z');

ROLLBACK;
