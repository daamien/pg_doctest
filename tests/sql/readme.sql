BEGIN;

CREATE SCHEMA doctest;
CREATE EXTENSION pgtap;
CREATE EXTENSION pg_doctest WITH SCHEMA doctest;

CREATE SCHEMA foo;

CREATE OR REPLACE FUNCTION foo.add ( a INT, b INT)
RETURNS INT LANGUAGE SQL
AS $$
    -- >>> SELECT foo.add(1,1)
    -- 2
    -- >>> SELECT foo.add(NULL,9)
    -- NULL
    -- >>> SELECT foo.add(4,2)
    -- 42
    SELECT a+b;
$$;

SELECT runtests('doctest'::NAME);

ROLLBACK;
