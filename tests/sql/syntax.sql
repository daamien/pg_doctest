BEGIN;

CREATE SCHEMA test;
CREATE EXTENSION pgtap;
CREATE EXTENSION pg_doctest SCHEMA test;

CREATE SCHEMA foo;

-- These tests are correct
CREATE OR REPLACE FUNCTION foo.plus ( a INT, b INT)
RETURNS INT
AS $func$
-- >>>           select     foo.plus(1,1)
  -- 2
--
  -- >>>  SELECT foo.plus(NULL,9)
  -- NULL
        -- >>> SELECt  foo.plus(0,000)
        --       0
  SELECT a+b;
$func$
  LANGUAGE SQL
;

-- These tests will fail
CREATE OR REPLACE FUNCTION foo.minus ( a INT, b INT)
RETURNS INT
AS $func$

  -- >>> SELECT foo.minus(1,1)
  -- 0
  -- >>> SELECT foo.minus(NULL,9)
  -- NULL

  -- This bug is here on purpose, DO NOT FIX :)
  SELECT a+b;
$func$
  LANGUAGE SQL
;


-- Test a multiline statement
-- Test function in multiple schemas

CREATE SCHEMA z;
CREATE FUNCTION z.is_bob(n TEXT)
RETURNS BOOLEAN LANGUAGE SQL
AS $$
-- >>> WITH person AS (
-- >>>   SELECT 'alice' AS firstname
-- >>> )
-- >>> SELECT z.is_bob(p.firstname)
-- >>> FROM person p;
-- false

SELECT n = 'bob'
$$;


SELECT runtests('test'::NAME);

ROLLBACK;
